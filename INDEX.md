# EDLIN

Active development at [https://sourceforge.net/projects/freedos-edlin/](https://sourceforge.net/projects/freedos-edlin/)

## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## EDLIN.LSM

<table>
<tr><td>title</td><td>EDLIN</td></tr>
<tr><td>version</td><td>2.19</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-03-21</td></tr>
<tr><td>description</td><td>The edlin program is the FreeDOS standard line editor. (UPX Compressed)</td></tr>
<tr><td>keywords</td><td>edit, editor, line editor</td></tr>
<tr><td>author</td><td>Gregory Pietsch &lt;gpietsch -at- comcast.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Gregory Pietsch &lt;gpietsch -at- users.sourceforge.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://sourceforge.net/projects/freedos-edlin</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/edlin</td></tr>
<tr><td>original&nbsp;site</td><td>http://sourceforge.net/projects/freedos-edlin</td></tr>
<tr><td>platforms</td><td>DOS (Microsoft Visual C++ in C mode, Borland C++), FreeDOS, OpenWatcom, Linux (gcc)</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Edlin</td></tr>
</table>
