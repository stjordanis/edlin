/* edlin.sl - Slovene-language messages file                                  */
/*                                                                             */
/* Author: Matej Horvat (http://matejhorvat.si/)                              */

#ifndef MSGS_H
#define MSGS_H

#define YES	"Dd"
#define G00001	"\040:\040"
#define G00002	"V redu?\040"
#define G00003	"Napaka v vrnosu"
#define G00004	"%s: %lu vrstica prebrana\n"
#define G00005	"%s: %lu vrstic prebranih\n"
#define G00006	"%s: %lu vrstica zapisana\n"
#define G00007	"%s: %lu vrstic zapisanih\n"
#define G00008	"%lu:%c%s\n"
#define G00009	"Pritisnite Enter za nadaljevanje"
#define G00010	"%lu:\040"
#define G00011	"Not found"
#define G00012	"%lu: %s\n"
#define G00013	"\nEDLIN razume naslednje ukaze:\n"
#define G00014	"#               uredi eno vrstico        [#],[#],#m      premakni"
#define G00015	"a               dodaj na konec datoteke  [#][,#]p        stran"
#define G00016	"[#],[#],#,[#]c  kopiraj                  q               kon\237aj"
#define G00017	"[#][,#]d        izbri\347i                  [#][,#][?]r$,$  zamenjaj"
#define G00018	"e<>             shrani in kon\237aj         [#][,#][?]s$    i\347\237i"
#define G00019	"[#]i            vstavi                   [#]t<>          prenesi"
#define G00020	"[#][,#]l        izpi\347i                   [#]w<>          shrani\n"
#define G00021	"kjer je zgoraj $ niz, <> ime datoteke, # \347tevilo (ki je lahko . za"
#define G00022	"trenutno vrstico, $ za zadnjo vrstico $=last line, ali pa \347tevilo + ali"
#define G00023	"- \347e eno \347tevilo).\n"
#define G00024	", (C) 2003 Gregory Pietsch"
#define G00025	"Ta program je razpe\237avan BREZ KAKR\346NEGAKOLI JAMSTVA."
#define G00026	"Razpe\237avate ga lahko pod pogoji druge, ali, \237e"
#define G00027	"\247elite, katerekoli novej\347e verzije Splo\347nega"
#define G00028	"dovoljenja GNU (GNU GPL)."
#define G00029	""
#define G00030	"Ni dovolj spomina"
#define G00031	"Napaka v dol\247ini niza"
#define G00032	"Napaka v mestu v nizu"
#define G00033	"Neveljaven ukaz; vnesite ? za pomo\237."
#define G00034	"Zahtevano je ime datoteke."
#define G00035	"Za\237asni pomnilnik prevelik"
#define G00036	"Neveljavno mesto v za\237asnem pomnilniku"
#define G00037	"Napaka: %s\n"

/* END OF FILE                                                                */

#endif

/* END OF FILE */
