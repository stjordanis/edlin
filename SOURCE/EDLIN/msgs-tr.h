/* edlin.tr - Turkish-language messages file                                   */
/*                                                                             */
/* Author: Gregory Pietsch                                                     */
/* Translator: Ercan Ersoy                                                     */
/*                                                                             */
/* DESCRIPTION:                                                                */
/*                                                                             */
/* This file contains #defines for all the message strings in edlin.           */
/* For internationalization fun, just translate the messages in this           */
/* file.                                                                       */
/*                                                                             */
/* The strings were generated using ESR's cstrings program and moved           */
/* here.                                                                       */

#ifndef MSGS_H
#define MSGS_H

#define YES	"Ee"
#define G00001	"\040:\040"
#define G00002	"Tamam?\040"
#define G00003	"Giri\237 hatas\215"
#define G00004	"%s: %lu sat\215r okunuyor\n"
#define G00005	"%s: %lu sat\215r okunuyor\n"
#define G00006	"%s: %lu sat\215r yaz\215l\215yor\n"
#define G00007	"%s: %lu sat\215r yaz\215l\215yor\n"
#define G00008	"%lu:%c%s\n"
#define G00009	"S\201rd\201rmek i\207in <Enter>'e bas\215n\215z"
#define G00010	"%lu:\040"
#define G00011	"Bulunmad\215"
#define G00012	"%lu: %s\n"
#define G00013	"\nedlin izleyen alt komutlara sahiptir:\n"
#define G00014	"#                bir tek sat\215r\215 d\201zenlemek   [#],[#],#m       ta\237\215mak"
#define G00015	"a                eklemek                     [#][,#]p         sayfa"
#define G00016	"[#],[#],#,[#]c   kopyalamak                  q                \207\215kmak"
#define G00017	"[#][,#]d         silmek                      [#][,#][?]r$,$   yenilemek"
#define G00018	"e<>              son (yazma ile \207\215kma)       [#][,#][?]s$     arama"
#define G00019	"[#]i             insert                      [#]t<>           aktarma"
#define G00020	"[#][,#]l         listelemek                  [#]w<>           yazma\n"
#define G00021	"$'nin bir karakter dizisi \201zerine ise <> bir dosya ismidir,"
#define G00022	"# bir numarad\215r (.=ge\207erli sat\215r, $=son sat\215r,"
#define G00023	"ya da + say\215 veya - di\247er say\215).\n"
#define G00024	", telif hakk\215 (c) 2003 Gregory Pietsch"
#define G00025	"Bu program KES\230NL\230KLE TEM\230NAT VERMEME ile gelir."
#define G00026	"Bu \224zg\201r yaz\215l\215md\215r ve bunu GNU Genel Kamu Lisans\215'n\215n"
#define G00027	"ko\237ullar\215 (lisans\215n s\201r\201m 2 ya da sizin se\207iminize g\224re"
#define G00028	"herhangi sonra olan s\201r\201m) kapsam\215nda yeniden da\247\215tmaya"
#define G00029	"hakk\215n\215z vard\215r.\n"
#define G00030	"Bellek yetersiz"
#define G00031	"Karakter dizisi uzunlu\247u hatas\215"
#define G00032	"Karakter dizisi konumu hatas\215"
#define G00033	"Ge\207ersiz kullan\215c\215 giri\237i, yard\215m i\207in ? kullan\215n."
#define G00034	"Dosya ismi yok"
#define G00035	"Tampon \207ok b\201y\201k"
#define G00036	"Ge\207ersiz tampon konumu"
#define G00037	"HATA: %s\n"

/* END OF FILE                                                                 */

#endif

/* END OF FILE */
